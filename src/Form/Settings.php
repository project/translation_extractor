<?php

namespace Drupal\translation_extractor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class Settings.
 *
 * The module's settings form.
 *
 * @package Drupal\translation_extractor\Form
 */
class Settings extends ConfigFormBase {

  use StringTranslationTrait;
  use MultivalueRowTrait;

  /**
   * The name of the settings file.
   *
   * @var string
   */
  const SETTINGSFILE = 'translation_extractor.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGSFILE];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::SETTINGSFILE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the actual configuration object.
    $config = $this->configFactory->get(self::SETTINGSFILE);

    $form['#tree'] = TRUE;

    // Create the container.
    $form['settings'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Search patterns', [], ['context' => 'translation_extractor']),

      'fileExtensions' => [
        '#type' => 'textarea',
        '#title' => $this->t('File extensions', [], ['context' => 'translation_extractor']),
        '#description' => $this->t('The file extensions to include in the scan. One per line including the leading dot.', [], ['context' => 'translation_extractor']),
        '#default_value' => implode(PHP_EOL, $config->get('fileExtensions')),
        '#element_validate' => [[$this, 'validateFileExtensions']],
        '#required' => TRUE,
        '#rows' => 7,
      ],

      'searchPatterns' => [
        '#type' => 'container',
      ],
    ];

    // Add the form rows.
    $this->createMultivalueFormPortion(
      $form['settings']['searchPatterns'],
      'searchPatterns',
      $form_state,
      $config->get('searchPatterns'),
      'No patterns defined. Use the "Add pattern" to define new search patterns.'
    );

    // Return the complete form.
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate the file extensions entered.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormStateInterface object.
   */
  public function validateFileExtensions(array $element, FormStateInterface $form_state) {
    $fileExtensions = $this->preprocessFileExtensions($element["#value"]);
    $faultyExtensionFound = FALSE;
    array_walk($fileExtensions, function ($extension) use (&$faultyExtensionFound) {
      if (!preg_match('~^\..+$~i', $extension)) {
        $faultyExtensionFound = TRUE;
      }
    });
    if ($faultyExtensionFound) {
      $form_state->setError(
        $element,
        sprintf('Please check the file extensions entered.')
      );
    }
  }

  /**
   * Element validator function to make sure the input contains valid pattern.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateRegularExpression(array $element, FormStateInterface $form_state) {
    try {
      if (@preg_match($element["#value"], 'This is a string just for testing purporses.') === FALSE) {
        throw new \Exception();
      }
    }
    catch (\Exception $e) {
      $form_state->setError(
        $element,
        sprintf('The "%s" element does not contain valid PCRE pattern.', $element['#title'])
      );
    }
  }

  /**
   * Validates the match number of a potential plural form.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormStateInterface object.
   */
  public function validatePluralMatchNumber(array $element, FormStateInterface $form_state) {
    $searchPatterns = $this->getData('searchPatterns', $form_state->getUserInput());
    if ((bool) $searchPatterns[$element['#delta']]['row2']['contains_plural']) {
      $matchNumberTranslationString = (int) $searchPatterns[$element['#delta']]['row1']['match'];
      $matchNumberPluralString = (int) $searchPatterns[$element['#delta']]['row2']['match_plural'];
      if ($matchNumberPluralString <= $matchNumberTranslationString) {
        $form_state->setError(
          $element,
          'The match number of the plural form has to be greater than the match number of the translation string itself.'
        );
      }
    }
  }

  /**
   * Validates the match number of a potential context value.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormStateInterface object.
   */
  public function validateContextMatchNumber(array $element, FormStateInterface $form_state) {
    $delta = $element['#delta'];
    $searchPatterns = $this->getData('searchPatterns', $form_state->getUserInput());
    if ((bool) $searchPatterns[$delta]['row3']['contains_context']) {
      $matchNumberTranslationString = (int) $searchPatterns[$delta]['row1']['match'];
      $containsPlural = (bool) $searchPatterns[$delta]['row2']['contains_plural'];
      $matchNumberPluralString = (int) $searchPatterns[$delta]['row2']['match_plural'];
      $matchNumberContextValue = (int) $searchPatterns[$delta]['row3']['match_context'];
      if (
        (!$containsPlural && $matchNumberContextValue <= $matchNumberTranslationString) ||
        ($containsPlural && $matchNumberContextValue <= $matchNumberPluralString)
      ) {
        $form_state->setError(
          $element,
          sprintf(
            'The match number of the context value has to be greater than the match number of the %s string.',
            $containsPlural ? 'plural' : 'translation'
          ),
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getInputRow($delta, array $row_defaults, FormStateInterface $form_state) {
    return [
      'row1' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['inlineInputs']],
        'pattern' => [
          '#type' => 'textfield',
          '#title' => $this->t('PCRE pattern', [], ['context' => 'translation_extractor']),
          '#description' => $this->t('Enter the expression including delimiters and flags', [], ['context' => 'translation_extractor']),
          '#default_value' => $row_defaults['pattern'],
          '#size' => 100,
          '#maxlength' => 500,
          '#required' => TRUE,
          '#element_validate' => [[$this, 'validateRegularExpression']],
        ],
        'match' => [
          '#type' => 'number',
          '#title' => $this->t('Match number', [], ['context' => 'translation_extractor']),
          '#description' => $this->t('The capture group number of the string', [], ['context' => 'translation_extractor']),
          '#default_value' => $row_defaults['match'],
          '#min' => 1,
          '#max' => 20,
          '#step' => 1,
          '#required' => TRUE,
        ],
      ],
      'row2' => [
        '#type' => 'container',
        'contains_plural' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Contains plural', [], ['context' => 'translation_extractor']),
          '#default_value' => $row_defaults['contains_plural'] ? TRUE : FALSE,
          '#attributes' => [
            'data-delta' => $delta,
          ],
        ],
        'match_plural' => [
          '#type' => 'number',
          '#title' => $this->t('Match number (plural form)', [], ['context' => 'translation_extractor']),
          '#delta' => $delta,
          '#default_value' => $row_defaults['match_plural'] ?: NULL,
          '#min' => 1,
          '#max' => 20,
          '#step' => 1,
          '#states' => [
            'visible' => ['input[type="checkbox"][name*=contains_plural][data-delta=' . $delta . ']' => ['checked' => TRUE]],
            'required' => ['input[type="checkbox"][name*=contains_plural][data-delta=' . $delta . ']' => ['checked' => TRUE]],
          ],
          '#element_validate' => [[$this, 'validatePluralMatchNumber']],
        ],
      ],
      'row3' => [
        '#type' => 'container',
        'contains_context' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Contains (potential) translation context', [], ['context' => 'translation_extractor']),
          '#default_value' => $row_defaults['contains_context'] ? TRUE : FALSE,
          '#attributes' => [
            'data-delta' => $delta,
          ],
        ],
        'match_context' => [
          '#type' => 'number',
          '#title' => $this->t('Match number (context value)', [], ['context' => 'translation_extractor']),
          '#default_value' => $row_defaults['match_context'] ?: NULL,
          '#element_validate' => [[$this, 'validateContextMatchNumber']],
          '#delta' => $delta,
          '#min' => 1,
          '#max' => 20,
          '#step' => 1,
          '#states' => [
            'visible' => ['input[type="checkbox"][name*=contains_context][data-delta=' . $delta . ']' => ['checked' => TRUE]],
            'required' => ['input[type="checkbox"][name*=contains_context][data-delta=' . $delta . ']' => ['checked' => TRUE]],
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDataToAdd($property, array $current_state, array $user_input, $addSelectorValue, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function allowMultipleEmptyAdds($property) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getRowType($property) {
    return 'fieldgroup';
  }

  /**
   * {@inheritdoc}
   */
  protected function getRowTitle($property) {
    return 'Pattern';
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddRowButtonTitle($property) {
    return 'Add pattern';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get an editable instance of the settings.
    $config = $this->configFactory->getEditable(static::SETTINGSFILE);

    // Extract the setting values from the form state.
    $formValues = $form_state->getValue('settings');

    // Prepare the configured file extensions.
    $fileExtensions = $this->preprocessFileExtensions($formValues['fileExtensions']);
    $config->set('fileExtensions', $fileExtensions);

    $searchPatterns = $this->getData('searchPatterns', $form_state->getUserInput());
    $searchPatterns = array_map(
      function ($pattern) {
        return [
          'pattern' => $pattern['row1']['pattern'],
          'match' => (int) $pattern['row1']['match'],
          'contains_plural' => (bool) $pattern['row2']['contains_plural'],
          'match_plural' => (bool) $pattern['row2']['contains_plural'] ? (int) $pattern['row2']['match_plural'] : 0,
          'contains_context' => (bool) $pattern['row3']['contains_context'],
          'match_context' => (bool) $pattern['row3']['contains_context'] ? (int) $pattern['row3']['match_context'] : 0,
        ];
      },
      $searchPatterns
    );
    $config->set('searchPatterns', $searchPatterns);

    // Save the Configuration.
    $config->save();

    // Delegate to parent.
    parent::submitForm($form, $form_state);
  }

  /**
   * Transforms the value of the file extension fields into an array.
   *
   * @param string $fileExtensions
   *   The extensions as a string.
   *
   * @return array
   *   The extensions as an array.
   */
  private function preprocessFileExtensions($fileExtensions) {
    $fileExtensions = explode(PHP_EOL, trim($fileExtensions));
    array_walk($fileExtensions, function (&$item) {
      $item = trim($item, "\n\r\t");
    });
    array_filter($fileExtensions);
    return $fileExtensions;
  }

}
