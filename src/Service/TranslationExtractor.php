<?php

namespace Drupal\translation_extractor\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Route;

/**
 * Class TranslationExtractor.
 *
 * @package Drupal\translation_extractor\Service
 */
class TranslationExtractor implements TranslationExtractorInterface {

  use StringTranslationTrait;

  /**
   * The currently processed request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Drupal's language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Private temp store object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * Cache service provided by Drupal.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * A list of available modules.
   *
   * @var \Drupal\Core\Extension\Extension[]
   */
  protected $moduleList;

  /**
   * TranslationExtractor constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack object.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Drupal's language mamager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store
   *   Private temp store object.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache service provided by Drupal.
   */
  public function __construct(
    RequestStack $request_stack,
    LanguageManagerInterface $language_manager,
    PrivateTempStoreFactory $private_temp_store,
    CacheBackendInterface $cache_backend
  ) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->languageManager = $language_manager;
    $this->privateTempStore = $private_temp_store->get('translation_extractor');
    $this->cacheBackend = $cache_backend;

    if (($cache = $this->cacheBackend->get('translation_extractor.moduleList')) === FALSE) {
      $this->moduleList = array_filter(
        system_rebuild_module_data(),
        function (Extension $module) {
          return !preg_match('~^core~', $module->getPath());
        }
      );
      $this->cacheBackend->set('translation_extractor.moduleList', $this->moduleList, time() + 120);
    }
    else {
      $this->moduleList = $cache->data;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = [];

    $routes['translation_extractor.scanResults'] = new Route(
      '/admin/config/regional/translation_extractor/{module}/results/{language}',
      [
        '_title' => 'Scan results',
        '_controller' => 'translation_extractor.scanner:scanResults',
        'language' => $this->languageManager->getCurrentLanguage()->getId(),
      ],
      [
        '_permission'  => 'use translation_extractor',
        'module' => '\\w+',
        'language' => '\\w+',
      ]
    );

    $routes['translation_extractor.scanResults.asFile'] = new Route(
      '/admin/config/regional/translation_extractor/{module}/download/{language}',
      [
        '_title' => 'Scan results',
        '_controller' => 'translation_extractor.scanner:poFile',
        'language' => $this->languageManager->getCurrentLanguage()->getId(),
      ],
      [
        '_permission'  => 'use translation_extractor',
        'module' => '\\w+',
        'language' => '\\w+',
      ]
    );

    return $routes;
  }

  /**
   * {@inheritdoc}
   */
  public function scan(ImmutableConfig $settings, $module) {
    // Extract the settings.
    $fileTypesToScan = $settings->get('fileExtensions');
    $searchPatterns = $settings->get('searchPatterns');

    // Prepare the file mask.
    $mask = sprintf('~(?:%s)~i', implode('|', array_map(function ($ext) {
      return preg_quote($ext, '~');
    }, $fileTypesToScan)));

    // Get the module data.
    $module = $this->moduleList[$module];

    // The directory prefix.
    $moduleDirectory = sprintf('%s/%s', DRUPAL_ROOT, $module->getPath());

    // Scan the given directory for files matching the mask.
    $files = file_scan_directory($moduleDirectory, $mask);

    // Sort files to have the same order on different OS.
    ksort($files);

    // Prepare a container to hold the scan results.
    $translationStrings = [];

    // Process each file found.
    foreach ($files as $file) {

      // Get the file's contents.
      $content = file_get_contents($file->uri);

      // Apply each defined pattern to the source.
      foreach ($searchPatterns as $patternDefinition) {

        // Apply the current pattern.
        preg_match_all($patternDefinition['pattern'], $content, $matches, PREG_OFFSET_CAPTURE);

        // Remember the results (if any).
        if (
          !empty($matches[$patternDefinition['match']]) &&
          strlen(trim($matches[$patternDefinition['match']][0][0]))
        ) {

          // For easier reading...
          $matchesFound = &$matches[$patternDefinition['match']];

          // Holds the current results...
          $result = [];

          // Preprocess the offsets.
          foreach ($matchesFound as $delta => &$match) {

            // Calculate the line number of the match.
            list($before) = str_split($content, $match[1]);
            $before = str_replace("\r\n", "\n", $before);
            $line_number = strlen($before) - strlen(str_replace("\n", '', $before)) + 1;
            $result[$delta]['line'] = sprintf(
              '#: %s:%d',
              str_replace("$moduleDirectory/", '', $file->uri),
              $line_number
            );

            if ($patternDefinition['contains_context'] && !empty($matches[$patternDefinition['match_context']][0][0])) {
              $result[$delta]['msgctxt'] = $matches[$patternDefinition['match_context']][0][0];
            }

            $result[$delta]['msgid'] = $match[0];

            $result[$delta]['has_plural'] = $patternDefinition['contains_plural'];
            if ($patternDefinition['contains_plural']) {
              $result[$delta]['msgid_plural'] = $matches[$patternDefinition['match_plural']][$delta][0];
            }
          }

          // Merge the new results.
          $translationStrings = array_merge($translationStrings, $result);
        }
      }
    }

    // Save the strings found.
    $this->privateTempStore->set('moduleScanned', $module);
    $this->privateTempStore->set('translationStrings', $translationStrings);
  }

  /**
   * {@inheritdoc}
   */
  public function scanResults() {
    // Determine the language that was requested for the translations.
    $langcode = $this->currentRequest->attributes->get('language');
    $languageRequested = $this->languageManager->getLanguage($langcode);

    // Get all installed languages for the language selector.
    $languagesInstalled = $this->languageManager->getLanguages();
    array_walk($languagesInstalled, function (Language &$item) {
      $item = $item->getName();
    });

    // Return the processed results.
    return [
      'results' => [
        '#type' => 'textarea',
        '#title' => $this->t('Scan results', [], ['context' => 'translation_extractor']),
        '#description' => $this->t(
          'Save as %pofilename',
          [
            '%pofilename' => Link::createFromRoute(
              sprintf(
                '%1$s/files/translations/%1$s.%2$s.po',
                $this->privateTempStore->get('moduleScanned')->getName(),
                $languageRequested->getId()
              ),
              'translation_extractor.scanResults.asFile',
              [
                'module' => $this->currentRequest->attributes->get('module'),
                'language' => $langcode,
              ]
            )->toString(),
          ],
          [
            'context' => 'translation_extractor',
          ]
        ),
        '#description_display' => 'after',
        '#value' => $this->getTranslationTemplate($languageRequested, $languagesInstalled),
        '#rows' => 30,
        '#attributes' => [
          'readonly' => 'readonly',
        ],
      ],
      'languageSwitcher' => [
        '#type' => 'select',
        '#id' => 'languageSwitcher',
        '#title' => $this->t('Language', [], ['context' => 'translation_extractor']),
        '#description' => $this->t('Select the language to generate the PO file for.', [], ['context' => 'translation_extractor']),
        '#description_display' => 'before',
        '#options' => $languagesInstalled,
        '#value' => $langcode,
      ],
      '#attached' => [
        'library' => ['translation_extractor/resultpage'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function poFile() {
    $langcode = $this->currentRequest->attributes->get('language');
    $languageRequested = $this->languageManager->getLanguage($langcode);
    $languagesInstalled = $this->languageManager->getLanguages();
    array_walk($languagesInstalled, function (Language &$item) {
      $item = $item->getName();
    });
    $filename = sprintf('%s.%s.po', $this->privateTempStore->get('moduleScanned')->getName(), $langcode);
    $target = sprintf('public://%s', $filename);

    $handle = fopen($target, 'c');
    fwrite($handle, $this->getTranslationTemplate($languageRequested, $languagesInstalled));
    fclose($handle);

    $response = new BinaryFileResponse($target);
    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
    $response->deleteFileAfterSend(TRUE);
    return $response;
  }

  /**
   * Compiles a PO template for the language requested.
   *
   * @param \Drupal\Core\Language\LanguageInterface $languageRequested
   *   The language the PO template was requested for.
   * @param string[] $languagesInstalled
   *   An array containing the names of the languages installed,
   *   keyed by their langcode.
   *
   * @return string
   *   The compiled PO template for the requested language
   */
  protected function getTranslationTemplate(LanguageInterface $languageRequested, array $languagesInstalled) {
    // Get the native language names.
    $nativeLanguages = $this->languageManager->getNativeLanguages();

    // Prepare the PO template.
    $poTemplate = [
      '#',
      sprintf(
        '# Translations for Module "%s" for language "%s"',
        $this->privateTempStore->get('moduleScanned')->info['name'],
        ucfirst($nativeLanguages[$languageRequested->getId()]->getName())
      ),
      '#',
    ];

    // Get the strings that matched the patterns.
    $translationStringsFound = $this->privateTempStore->get('translationStrings');

    // Process each string found.
    foreach ($translationStringsFound as $string) {
      $poTemplate[] = '';
      $poTemplate[] = $string['line'];
      if (isset($string['msgctxt'])) {
        $poTemplate[] = sprintf('msgctxt "%s"', preg_replace('~(?<!\\\\)"~', '\"', $string['msgctxt']));
      }
      $poTemplate[] = sprintf('msgid "%s"', preg_replace('~(?<!\\\\)"~', '\"', $string['msgid']));
      if ($string['has_plural']) {
        $poTemplate[] = sprintf('msgid_plural "%s"', preg_replace('~(?<!\\\\)"~', '\"', $string['msgid_plural']));
      }
      $translatedString = isset($string['msgctxt'])
        ? $this->t($string['msgid'], [], ['context' => $string['msgctxt'], 'langcode' => $languageRequested->getId()])
        : $this->t($string['msgid'], [], ['langcode' => $languageRequested->getId()]);
      $poTemplate[] = sprintf(
        'msgstr%s "%s"',
        $string['has_plural'] ? '[0]' : '',
        $translatedString == $string['msgid'] ? '' : preg_replace('~(?<!\\\\)"~', '\"', $translatedString)
      );
      if ($string['has_plural']) {
        $translatedString = isset($string['msgctxt'])
          ? $this->t($string['msgid_plural'], [], ['context' => $string['msgctxt'], 'langcode' => $languageRequested->getId()])
          : $this->t($string['msgid_plural'], [], ['langcode' => $languageRequested->getId()]);
        $poTemplate[] = sprintf(
          'msgstr[1] "%s"',
          $translatedString == $string['msgid_plural'] ? '' : preg_replace('~(?<!\\\\)"~', '\"', $translatedString)
        );
      }
    }

    return implode(PHP_EOL, $poTemplate);
  }

}
