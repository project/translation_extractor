<?php

namespace Drupal\translation_extractor\Service;

use Drupal\Core\Config\ImmutableConfig;

/**
 * Interface TranslationExtractorInterface.
 *
 * Defines the API of the extraction service.
 *
 * @package Drupal\translation_extractor\Service
 */
interface TranslationExtractorInterface {

  /**
   * Scans the given module according to the settings configured.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $configuration
   *   The module settings.
   * @param string $module
   *   The module to scan.
   */
  public function scan(ImmutableConfig $configuration, $module);

  /**
   * Displays the scan results.
   *
   * @return array
   *   The render array containing the results page.
   */
  public function scanResults();

  /**
   * Generates a poFile for download.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   A binary file response object initialising a download.
   */
  public function poFile();

}
